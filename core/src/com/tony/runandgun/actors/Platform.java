package com.tony.runandgun.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.tony.runandgun.utils.Assets;

public class Platform extends Image {

    // Static Values
    public static final float SCALE = 0.4f;

    protected Rectangle mBounds;

    private final int mLength;
    private final float mWidth;
    private final float mHeight;

    public Platform(float x, float y, int length) {
        super();
        setPosition(x, y);
        mLength = length;
        mWidth = Assets.MIDDLE_PLATFORM.getWidth() * SCALE;
        mHeight = Assets.MIDDLE_PLATFORM.getHeight() * SCALE;
        mBounds = new Rectangle(x, y, mWidth * length, mHeight);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        validate();

        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

        for (int i = 0; i < mLength; i++) {

            float x = getX() + i * mWidth;
            float y = getY();

            if (i == 0) {
                batch.draw(Assets.LEFT_PLATFORM, x, y, mWidth, mHeight);
            } else if (i == mLength - 1) {
                batch.draw(Assets.RIGHT_PLATFORM, x, y, mWidth, mHeight);
            } else {
                batch.draw(Assets.MIDDLE_PLATFORM, x, y, mWidth, mHeight);
            }
        }
    }

    public Rectangle getBounds() {
        return mBounds;
    }

    public float getWidth() {
        return mWidth;
    }

    public float getHeight() {
        return mHeight;
    }
}
