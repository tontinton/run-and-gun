package com.tony.runandgun.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.tony.runandgun.utils.Assets;

abstract public class AbstractScreen extends Stage implements Screen {

    private static final String TAG = "AbstractScreen";

    protected float mScreenTimer = 0;
    protected OrthographicCamera mCamera;

    protected Vector3 mMousePosition;

    protected AbstractScreen() {
        super(new StretchViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        mCamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        mCamera.position.set(mCamera.viewportWidth / 2f, mCamera.viewportHeight / 2f, 0);
        mCamera.update();
        getViewport().setCamera(mCamera);
    }

    public abstract void buildStage(Object... params);

    @Override
    public void show() {
        Gdx.input.setInputProcessor(this);
    }

    @Override
    public void resize(int width, int height) {
        getViewport().update(width, height, true);
        System.out.println(Gdx.graphics.getWidth() + ", " + Gdx.graphics.getHeight()); // TODO: delete this line
    }

    abstract public void update(float delta);

    protected void updateMousePosition() {
        mMousePosition = mCamera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
    }

    @Override
    public void render(float delta) {
        mScreenTimer += delta;

        update(delta);
        // Clear screen
        Gdx.gl.glClearColor(200f / 255f, 1, 150f / 255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        getBatch().begin();
        getBatch().draw(Assets.BACKGROUND, -getWidth() * 0.5f, -getHeight() * 0.5f, getWidth() * 2f, getHeight() * 2f);
        getBatch().end();

        // Calling to Stage methods
        super.act(delta);
        super.draw();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    protected float getMinY() {
        return -getHeight() / 2;
    }

    protected float getMinX() {
        return -getWidth() / 2;
    }

    protected float getMaxY() {
        return getHeight() * 1.5f;
    }

    protected float getMaxX() {
        return getWidth() * 1.5f;
    }
}
