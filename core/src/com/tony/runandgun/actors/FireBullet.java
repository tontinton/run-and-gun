package com.tony.runandgun.actors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class FireBullet extends DynamicImage {

    // Static Values
    public static final float SCALE = 2f;
    public static final float SPEED = 1500f;
    public static final float RATE_PER_SECOND = 2f;

    //Properties
    private double mAngle = 0;

    public FireBullet(String id, float x, float y, double direction, TextureRegion textureRegion) {
        super(id, textureRegion, x, y, SPEED, SCALE);
        mAngle = direction;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        setVelocity(delta, false);
        setPosition(getX() + mVelocity.x * (float) Math.cos(mAngle),
                getY() + mVelocity.y * (float) Math.sin(mAngle));
    }

    public boolean isOverlappingPlatform(Platform platform) {
        return getBounds().overlaps(platform.getBounds());
    }

    public double getAngle() {
        return mAngle;
    }
}
