package com.tony.runandgun.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.tony.runandgun.actors.Character;
import com.tony.runandgun.actors.DynamicImage;
import com.tony.runandgun.actors.FireBullet;
import com.tony.runandgun.actors.MyCharacter;
import com.tony.runandgun.actors.Platform;
import com.tony.runandgun.utils.Assets;
import com.tony.runandgun.utils.ServerConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class GameScreen extends AbstractScreen {

    private static final String TAG = "GameScreen";

    private static final float UPDATE_TIME = 1f / 120f; // 60 times per second
    private static final String SERVER_ADDRESS = "http://10.0.0.4:8080";

    float mTimeSinceLastServerUpdate;
    String mMyId;
    MyCharacter mCharacter;
    Socket mSocket;

    @Override
    public void buildStage(Object... params) {
        try {
            mSocket = IO.socket(SERVER_ADDRESS);
            mSocket.connect();
        } catch (URISyntaxException e) {
            Gdx.app.log("SocketIO", "Error connecting to server: ", e);
        }
        configSocketEvents();
    }

    private void configSocketEvents() {
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Gdx.app.log("SocketIO", "Connected");
                TextureRegion textureRegion = new TextureRegion(Assets.FRIENDLY);
                mCharacter = new MyCharacter(ServerConstants.DEFAULT_ID,
                        (float) Math.random() * (Gdx.graphics.getWidth() - textureRegion.getRegionWidth() - 600) + 300,
//                        300f,
                        1000f,
                        textureRegion);
                addActor(mCharacter);
                setKeyboardFocus(mCharacter);

                loadMap();
            }
        }).on(ServerConstants.Functions.SOCKET_ID, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                mMyId = data.getString(ServerConstants.ID);
                mCharacter.setId(mMyId);
                Gdx.app.log(ServerConstants.TAG, "My ID: " + mMyId);
            }
        }).on(ServerConstants.Functions.NEW_PLAYER, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String id = data.getString(ServerConstants.ID);
                Gdx.app.log("SocketIO", "New Player Connected: " + id);
                TextureRegion textureRegion = new TextureRegion(Assets.ENEMY);
                Character enemy = new Character(id, -Gdx.graphics.getWidth(),
                        -Gdx.graphics.getHeight(), textureRegion);
                addActor(enemy);
            }
        }).on(ServerConstants.Functions.PLAYER_DISCONNECTED, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String id = data.getString(ServerConstants.ID);
                Gdx.app.log(ServerConstants.TAG, "Enemy Disconnected: " + id);
                Character enemy = (Character) getDynamicImage(id);
                if (enemy != null) {
                    enemy.remove();
                }
            }
        }).on(ServerConstants.Functions.GET_PLAYERS, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONArray objects = (JSONArray) args[0];
                for (int i = 0; i < objects.length(); i++) {
                    float x = ((Double) objects.getJSONObject(i).getDouble(ServerConstants.X)).floatValue();
                    float y = ((Double) objects.getJSONObject(i).getDouble(ServerConstants.Y)).floatValue();
                    String enemyId = objects.getJSONObject(i).getString(ServerConstants.ID);
                    TextureRegion textureRegion = new TextureRegion(Assets.ENEMY);
                    Character enemy = new Character(enemyId, x, y, textureRegion);
                    addActor(enemy);
                }
            }
        }).on(ServerConstants.Functions.PLAYER_MOVED, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String id = data.getString(ServerConstants.ID);
                if (id.equals(mCharacter.getId())) {
                    return;
                }
                Double x = data.getDouble(ServerConstants.X);
                Double y = data.getDouble(ServerConstants.Y);
                Character character = (Character) getDynamicImage(id);
                if (character != null) {
                    character.setPosition(x.floatValue(), y.floatValue());
                } else {
                    Gdx.app.log(ServerConstants.TAG, "Couldn't find player: " + id);
                }
            }
        }).on(ServerConstants.Functions.PLAYER_SHOT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String id = data.getString(ServerConstants.ID);
                Double x = data.getDouble(ServerConstants.X);
                Double y = data.getDouble(ServerConstants.Y);
                Double angle = data.getDouble(ServerConstants.ANGLE);
                Integer side = data.getInt(ServerConstants.SIDE);
                TextureRegion textureRegion = new TextureRegion(Assets.FIRE_BULLET);
                FireBullet fireBullet = new FireBullet(id, x.floatValue(), y.floatValue(),
                        angle, textureRegion);

                if (side == 1) {
                    fireBullet.getTextureRegion().flip(true, false);
                    fireBullet.setX(fireBullet.getX() - fireBullet.getImageWidth());
                }
                addActor(fireBullet);
            }
        }).on(ServerConstants.Functions.PLAYER_SWITCH_SIDE, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String id = data.getString(ServerConstants.ID);
                Integer side = data.getInt(ServerConstants.SIDE);
                Character character = (Character) getDynamicImage(id);
                if (character != null) {
                    if (side == 0) {
                        character.setLookState(DynamicImage.LookState.RIGHT);
                    } else {
                        character.setLookState(DynamicImage.LookState.LEFT);
                    }
                } else {
                    Gdx.app.log(ServerConstants.TAG, "Couldn't find player: " + id);
                }
            }
        }).on(ServerConstants.Functions.PLAYER_HIT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String id = data.getString(ServerConstants.ID);
                DynamicImage image = getDynamicImage(id);
                if (image != null &&
                        image instanceof Character &&
                        !id.equals(mMyId)) {
                    ((Character) image).startAlphaAnimation();
                }
            }
        });
    }

    /**
     * This function is shit right now... :)
     */
    private void loadMap() {
        addActor(new Platform(getWidth() * 0.1f, getHeight() * 0.27f, 8));
        addActor(new Platform(getWidth() - getWidth() * 0.31f, getHeight() * 0.27f, 8));
        addActor(new Platform(getWidth() * 0.1f, getHeight() * 0.63f, 8));
        addActor(new Platform(getWidth() - getWidth() * 0.31f, getHeight() * 0.63f, 8));
        addActor(new Platform(getWidth() * 0.3f, getHeight() * 0.09f, 15));
        addActor(new Platform(getWidth() * 0.3668f, getHeight() * 0.45f, 10));
    }

    private void updateServerMovement(float delta) {
        mTimeSinceLastServerUpdate += delta;
        if (mTimeSinceLastServerUpdate >= UPDATE_TIME && mCharacter != null && mCharacter.hasMoved()) {
            JSONObject data = new JSONObject();
            try {
                data.put(ServerConstants.X, mCharacter.getX());
                data.put(ServerConstants.Y, mCharacter.getY());
                mSocket.emit(ServerConstants.Functions.PLAYER_MOVED, data);
            } catch (JSONException e) {
                Gdx.app.error(ServerConstants.TAG, "Error uploading movement data", e);
            }
            mTimeSinceLastServerUpdate = 0;
        }
    }

    private void updateServerBulletShot(FireBullet fireBullet, DynamicImage.LookState lookState) {
        int side = lookState == DynamicImage.LookState.RIGHT ? 0 : 1;
        JSONObject data = new JSONObject();
        data.put(ServerConstants.X, fireBullet.getX());
        data.put(ServerConstants.Y, fireBullet.getY());
        data.put(ServerConstants.ANGLE, fireBullet.getAngle());
        data.put(ServerConstants.SIDE, side);
        mSocket.emit(ServerConstants.Functions.PLAYER_SHOT, data);
    }

    private void updateServerBulletRemoved(FireBullet fireBullet) {
        JSONObject data = new JSONObject();
        data.put(ServerConstants.ID, fireBullet.getId());
        mSocket.emit(ServerConstants.Functions.BULLET_REMOVED, data);
    }

    private void updateServerPlayerHit(Character character) {
        JSONObject data = new JSONObject();
        data.put(ServerConstants.ID, character.getId());
        mSocket.emit(ServerConstants.Functions.PLAYER_HIT, data);
    }

    private void updateServerPlayerSwitchSide(DynamicImage.LookState lookState) {
        int side = lookState == DynamicImage.LookState.RIGHT ? 0 : 1;
        JSONObject data = new JSONObject();
        data.put(ServerConstants.ID, mCharacter.getId());
        data.put(ServerConstants.SIDE, side);
        mSocket.emit(ServerConstants.Functions.PLAYER_SWITCH_SIDE, data);
    }

    @Override
    public void update(float delta) {
        // Better performance
        if (getActors().size == 0) {
            return;
        }

        updateMousePosition();

        if (Gdx.input.isKeyPressed(Input.Keys.I)) {
            mCamera.zoom -= 0.01f;
        } else if (Gdx.input.isKeyPressed(Input.Keys.O)) {
            mCamera.zoom += 0.01f;
        }

        updateServerMovement(delta);

        handleBullets();
        handleCharacter();
        handlePlatforms();
        handleCamera();
    }

    /**
     * Check if any of the bullets went out of the screen and if any of the bullets hit a character.
     */
    private void handleBullets() {
        for (int i = getActors().size - 1; i >= 0; i--) {

            if (i >= getActors().size) {
                // This happens when your character dies
                continue;
            }

            Actor bulletActor = getActors().get(i);
            if (!(bulletActor instanceof FireBullet)) {
                continue;
            }

            FireBullet fireBullet = (FireBullet) bulletActor;

            if (fireBullet.getX() > getMaxX() ||
                    fireBullet.getY() > getMaxY() ||
                    fireBullet.getX() + fireBullet.getWidth() < getMinX() ||
                    fireBullet.getY() + fireBullet.getHeight() < getMinY()) {

                updateServerBulletRemoved(fireBullet);
                fireBullet.remove();
                continue;
            }

            // Check if the current bullet hit any character
            for (int j = getActors().size - 1; j >= 0; j--) {
                Actor characterActor = getActors().get(j);
                if (!(characterActor instanceof Character)) {
                    continue;
                }
                Character character = (Character) characterActor;

                if (fireBullet.getBounds().overlaps(character.getBounds()) &&
                        !fireBullet.getId().equals(character.getId()) &&
                        character.isAlive()) {

//                    character.remove();
                    fireBullet.remove();
                    updateServerBulletRemoved(fireBullet);
                    updateServerPlayerHit(character);
                    Gdx.app.log(TAG, "handleBullets: " + character.getId() + ", " + mMyId);
                    if (character.getId().equals(mMyId)) {
                        mCharacter.die();
                    }
                }
            }
        }
    }

    /**
     * Check if the character is able to shoot and if it orders to shoots.
     */
    private void handleCharacter() {
        if (mCharacter == null) {
            return;
        }

        // Check if the character went out of the screen
        if (mCharacter.getY() < getMinY()) {
            mCharacter.die();
        }

        // Check look state
        if (mCharacter.getLookState() != DynamicImage.LookState.RIGHT &&
                mMousePosition.x > mCharacter.getX() + mCharacter.getImageWidth() / 2) {

            mCharacter.setLookState(DynamicImage.LookState.RIGHT);
            updateServerPlayerSwitchSide(DynamicImage.LookState.RIGHT);

        } else if (mCharacter.getLookState() != DynamicImage.LookState.LEFT &&
                mMousePosition.x < mCharacter.getX() + mCharacter.getImageWidth() / 2) {

            mCharacter.setLookState(DynamicImage.LookState.LEFT);
            updateServerPlayerSwitchSide(DynamicImage.LookState.LEFT);

        }

        // Check if the character is shooting
        if (!mCharacter.isAbleToShoot() ||
                !Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            return;
        }

        float bulletX = mCharacter.getX() + mCharacter.getImageWidth() / 2;
        float bulletY = mCharacter.getY() + mCharacter.getImageHeight() / 4;
        double bulletAngle = getAngle(mCharacter.getX() + mCharacter.getImageWidth() / 2,
                mCharacter.getY() + mCharacter.getImageHeight() / 2,
                mMousePosition.x,
                mMousePosition.y);
        TextureRegion bulletTextureRegion = new TextureRegion(Assets.FIRE_BULLET);

        FireBullet fireBullet = new FireBullet(mMyId, bulletX, bulletY, bulletAngle, bulletTextureRegion);

        if (mCharacter.getLookState() == Character.LookState.LEFT) {
            fireBullet.getTextureRegion().flip(true, false);
            fireBullet.setX(fireBullet.getX() - fireBullet.getImageWidth());
        }

        updateServerBulletShot(fireBullet, mCharacter.getLookState());
        addActor(fireBullet);
        mCharacter.resetTimeSinceLastShot();
    }


    /**
     * Check if any of the bullets have overlapped any of the platform
     * and handle the character's position on a platform.
     */
    private void handlePlatforms() {
        for (int i = getActors().size - 1; i >= 0; i--) {
            Actor iActor = getActors().get(i);
            if (!(iActor instanceof Platform)) {
                continue;
            }
            Platform platform = (Platform) iActor;
            mCharacter.handlePlatform(platform);

            for (int j = getActors().size - 1; j >= 0; j--) {
                Actor jActor = getActors().get(j);
                if (!(jActor instanceof FireBullet)) {
                    continue;
                }
                FireBullet fireBullet = (FireBullet) jActor;
                if (fireBullet.isOverlappingPlatform(platform)) {
                    updateServerBulletRemoved(fireBullet);
                    fireBullet.remove();
                }
            }
        }
    }

    private void handleCamera() {
//        Gdx.app.log(TAG, "handleCamera: " + mCamera.zoom);
        float minX = getMaxX();
        float minY = getMaxY();
        float maxX = getMinX();
        float maxY = getMinY();

        int numCharacters = 0;

        for (int i = getActors().size - 1; i >= 0; i--) {
            Actor actor = getActors().get(i);
            if (!(actor instanceof Character)) {
                continue;
            }
            Character character = (Character) actor;
            numCharacters++;

            if (character.getX() < minX) {
                minX = character.getX();
            }
            if (character.getY() < minY) {
                minY = character.getY();
            }
            if (character.getX() > maxX) {
                maxX = character.getX();
            }
            if (character.getY() > maxY) {
                maxY = character.getY();
            }
        }
        if (numCharacters > 1) {
            mCamera.position.lerp(new Vector3((maxX + minX) / 2,
                    ((maxY + minY) / 2),
                    0), 0.05f);
            mCamera.zoom = Math.max(1f, ((maxX - minX) / getWidth()) * 1.3f);
            mCamera.update();
        }
//        else {
//            mCamera.position.set(getViewport().getScreenWidth() / 2,
//                    getViewport().getScreenHeight() / 2,
//                    0);
//            mCamera.zoom = 1;
//            mCamera.update();
//        }
    }

    public DynamicImage getDynamicImage(String id) {
        for (Actor actor : getActors()) {
            if (actor instanceof DynamicImage) {
                if (((DynamicImage) actor).getId().equals(id)) {
                    return (DynamicImage) actor;
                }
            }
        }
        return null;
    }

    public double getAngle(float x1, float y1, float x2, float y2) {
        double angle = Math.atan2(x2 - x1, y2 - y1);

        angle -= Math.PI / 2;
        if (angle < 0) {
            angle += Math.PI * 2;
        }
        angle = Math.PI * 2 - angle;

        return angle;
    }

    @Override
    public void dispose() {
        mSocket.disconnect();
        super.dispose();
    }
}
