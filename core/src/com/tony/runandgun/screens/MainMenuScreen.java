package com.tony.runandgun.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.tony.runandgun.actors.Platform;
import com.tony.runandgun.utils.Assets;

public class MainMenuScreen extends AbstractScreen {

    private Image mStartGameImage;

    @Override
    public void buildStage(Object... params) {
        mStartGameImage = new Image(new TextureRegion(Assets.START_GAME_BUTTON));
        mStartGameImage.setPosition(Gdx.graphics.getWidth() / 2 - mStartGameImage.getWidth() / 2,
                Gdx.graphics.getHeight() / 2 - mStartGameImage.getHeight() / 2);
        addActor(mStartGameImage);
        mStartGameImage.addListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ScreenManager.getInstance().showScreen(ScreenType.GAME);
                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }

    @Override
    public void update(float delta) {

    }
}
