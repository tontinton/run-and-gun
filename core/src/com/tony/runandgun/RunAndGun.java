package com.tony.runandgun;

import com.badlogic.gdx.Game;
import com.tony.runandgun.screens.ScreenType;
import com.tony.runandgun.screens.ScreenManager;
import com.tony.runandgun.utils.Assets;

public class RunAndGun extends Game {

    @Override
    public void create() {
        Assets.initialize();
        ScreenManager.getInstance().initialize(this);
        ScreenManager.getInstance().showScreen(ScreenType.MAIN_MENU);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        Assets.dispose();
    }
}
