package com.tony.runandgun.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.tony.runandgun.RunAndGun;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
		config.width = LwjglApplicationConfiguration.getDesktopDisplayMode().width;
		config.height = LwjglApplicationConfiguration.getDesktopDisplayMode().height;
		config.fullscreen = false;
//        config.width = 800;
//        config.height = 600;

		config.title = "Run And Gun made by Tony Solomonik";

		config.vSyncEnabled = false;
		config.foregroundFPS = 120;
		config.backgroundFPS = 120;

		new LwjglApplication(new RunAndGun(), config);
	}
}
