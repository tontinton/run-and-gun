package com.tony.runandgun.screens;

import com.badlogic.gdx.Screen;
import com.tony.runandgun.RunAndGun;

/**
 * Created by Tony Solomonik on 08/07/2016.
 */
public class ScreenManager {

    // Singleton: unique instance
    private static ScreenManager sInstance;

    // Reference to game
    private RunAndGun mGame;

    // Singleton: private constructor
    private ScreenManager() {
        super();
    }

    // Singleton: retrieve instance
    public static ScreenManager getInstance() {
        if (sInstance == null) {
            sInstance = new ScreenManager();
        }
        return sInstance;
    }

    // Initialization with the game class
    public void initialize(RunAndGun game) {
        this.mGame = game;
    }

    // Show in the game the screen which enum type is received
    public void showScreen(ScreenType screenType, Object... params) {
        // Show new screen
        AbstractScreen newScreen = screenType.getScreen(params);
        newScreen.buildStage();
        mGame.setScreen(newScreen);
    }
}
