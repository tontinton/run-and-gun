package com.tony.runandgun.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class MyCharacter extends Character {

    private static final String TAG = "MyCharacter";

    // Static Values
    public static final float SPEED = 650f;
    public static final float JUMP = 18f;
    public static final int PLATFORM_MAXIMUM = 50;
    public static final float REALLY_SMALL_NUMBER = (float) Math.pow(10, -1);

    // States
    public enum ActionState {
        JUMPING,
        FALLING,
        STANDING
    }

    private ActionState mActionState = ActionState.FALLING;

    // Properties
    private float mTimeSinceLastShot;
    private Platform mCurrentPlatform;

    public MyCharacter(String id, float x, float y, TextureRegion textureRegion) {
        super(id, x, y, textureRegion, SPEED);
        mTimeSinceLastShot = 0;
    }

    @Override
    public void act(float delta) {
        mRespawnTimer += delta;

        switch (getDeathState()) {
            case DEAD:
                respawn();
                super.act(delta);
                return;
            case RESPAWN:
                if (mRespawnTimer > RESPAWN_TIME) {
                    returnToLife();
                }
                super.act(delta);
                return;
        }

        // Check if falling before adding gravity
        if (mActionState != ActionState.FALLING &&
                mVelocity.y > 0) {
            mActionState = ActionState.FALLING;
        }

        setVelocity(delta, true);
        mTimeSinceLastShot += delta;

        // Jump
        if (mActionState == ActionState.STANDING &&
                Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            mActionState = ActionState.JUMPING;
            mVelocity.y = -JUMP;
        }

        if (mActionState != ActionState.STANDING) {
            setY(getY() - mVelocity.y);
        } else {
            mVelocity.y = 0;
        }

        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            setX(getX() - mVelocity.x);
        } else if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            setX(getX() + mVelocity.x);
        }

        super.act(delta);
    }

    public void handlePlatform(Platform platform) {
        if (!mBounds.overlaps(platform.getBounds())) {
            if (mActionState == ActionState.STANDING && platform == mCurrentPlatform) {
                mActionState = ActionState.FALLING;
                mCurrentPlatform = null;
            }
            return;
        }

        if (mActionState == ActionState.FALLING &&
                getY() >= platform.getY() + platform.getHeight() - PLATFORM_MAXIMUM) {
            // I want the character to still overlap the platform, that's why i subtract by a really small number.
            setY(platform.getY() + platform.getHeight() - REALLY_SMALL_NUMBER);
            mVelocity.y = 0;
            mActionState = ActionState.STANDING;
            mCurrentPlatform = platform;
        }
    }

    public boolean isAbleToShoot() {
        return mTimeSinceLastShot > 1f / FireBullet.RATE_PER_SECOND;
    }

    public void resetTimeSinceLastShot() {
        this.mTimeSinceLastShot = 0;
    }
}
