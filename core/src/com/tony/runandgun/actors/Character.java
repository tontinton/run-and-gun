package com.tony.runandgun.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class Character extends DynamicImage {

    private static final String TAG = "Character";

    public static final int RESPAWN_TIME = 2;

    protected enum DeathState {
        DEAD,
        RESPAWN,
        ALIVE
    }

    private DeathState mDeathState = DeathState.DEAD;

    protected float mRespawnTimer = 0;

    protected Character(String id, float x, float y, TextureRegion textureRegion, float speed) {
        super(id, textureRegion, x, y, speed);
    }

    public Character(String id, float x, float y, TextureRegion textureRegion) {
        super(id, textureRegion, x, y, 0);
    }

//    @Override
//    public void act(float delta) {
//        mRespawnTimer += delta;
//
//        Gdx.app.log(TAG, "act: " + mRespawnTimer + ", " + getDeathState());
//        switch (getDeathState()) {
//            case DEAD:
//                respawn();
//                return;
//            case RESPAWN:
//                if (mRespawnTimer > RESPAWN_TIME) {
//                    returnToLife();
//                }
//                return;
//        }
//
//        super.act(delta);
//    }

    protected DeathState getDeathState() {
        return mDeathState;
    }

    public boolean isAlive() {
        return mDeathState == DeathState.ALIVE;
    }

    public void die() {
        setPosition((float) Math.random() * (Gdx.graphics.getWidth() - getImageWidth() - 500) + 300,
                1000);
        mRespawnTimer = 0;
        mDeathState = DeathState.DEAD;
        mVelocity.set(0, 0);
    }

    public void respawn() {
        mDeathState = DeathState.RESPAWN;
        startRespawnAnimation();
    }

    public void startRespawnAnimation() {
        addAction(
                sequence(
                        alpha(0),
                        parallel(
                                fadeIn(RESPAWN_TIME),
                                moveTo(getX(), getY() - 200, RESPAWN_TIME, Interpolation.pow3Out)
                        )
                ));
    }

    public void startAlphaAnimation() {
        addAction(
                sequence(
                        alpha(0),
                        fadeIn(RESPAWN_TIME)
                ));
    }

    public void returnToLife() {
        mDeathState = DeathState.ALIVE;
    }
}
