package com.tony.runandgun.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class Assets {

    public static Texture BACKGROUND;

    public static Texture FRIENDLY;
    public static Texture ENEMY;
    public static Texture FIRE_BULLET;

    public static Texture LEFT_PLATFORM;
    public static Texture MIDDLE_PLATFORM;
    public static Texture RIGHT_PLATFORM;

    public static Texture START_GAME_BUTTON;

    public static BitmapFont FONT;

    public static void initialize() {
        BACKGROUND = new Texture("background.png");

        FRIENDLY = new Texture("frope.png");
        ENEMY = new Texture("enemy.png");
        FIRE_BULLET = new Texture("fire_bullet.png");

        LEFT_PLATFORM = new Texture("platform_left.png");
        MIDDLE_PLATFORM = new Texture("platform_middle.png");
        RIGHT_PLATFORM = new Texture("platform_right.png");

        START_GAME_BUTTON = new Texture("play_button.png");

        FONT = new BitmapFont(Gdx.files.internal("font.fnt"));
        FONT.setColor(Color.PINK);
    }

    public static void dispose() {
        START_GAME_BUTTON.dispose();
        FRIENDLY.dispose();
        ENEMY.dispose();
        FIRE_BULLET.dispose();
    }
}
