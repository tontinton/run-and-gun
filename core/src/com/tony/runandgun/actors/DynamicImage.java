package com.tony.runandgun.actors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.tony.runandgun.utils.World;

public class DynamicImage extends Image {

    // Static Values
    public static final float SCALE = 3f;

    // States
    public enum LookState {
        RIGHT,
        LEFT
    }

    // Properties
    protected float mSpeed;
    protected Vector2 mVelocity;
    protected TextureRegion mTextureRegion;
    protected LookState mLookState = LookState.RIGHT;
    protected Vector2 mPreviousPosition;
    protected String mId;
    protected Rectangle mBounds;

    public DynamicImage(String id, TextureRegion textureRegion, float x, float y, float speed) {
        super(textureRegion);
        mId = id;
        mTextureRegion = textureRegion;
        setPosition(x, y);
        setScale(SCALE);
        mBounds = new Rectangle(x, y,
                textureRegion.getRegionWidth() * SCALE, textureRegion.getRegionHeight() * SCALE);
        mSpeed = speed;
        mPreviousPosition = new Vector2();
        mVelocity = new Vector2();
    }

    public DynamicImage(String id, TextureRegion textureRegion, float x, float y, float speed, float scale) {
        super(textureRegion);
        mId = id;
        mTextureRegion = textureRegion;
        setPosition(x, y);
        setScale(scale);
        mBounds = new Rectangle(x, y,
                textureRegion.getRegionWidth() * SCALE, textureRegion.getRegionHeight() * SCALE);
        mSpeed = speed;
        mPreviousPosition = new Vector2();
        mVelocity = new Vector2();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        mBounds.setPosition(getX(), getY());
    }

    public boolean hasMoved() {
        if (mPreviousPosition.x != getX() || mPreviousPosition.y != getY()) {
            mPreviousPosition.set(getX(), getY());
            return true;
        }
        return false;
    }

    public void setVelocity(float delta, boolean isGravityForce) {
        mVelocity.x = mSpeed * delta;
        if (isGravityForce) {
            mVelocity.set(mSpeed * delta, mVelocity.y + World.GRAVITY * delta);
        } else {
            mVelocity.set(mSpeed * delta, mSpeed * delta);
        }
    }

    public void setLookState(LookState lookState) {
        this.mLookState = lookState;
        mTextureRegion.flip(true, false);
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getId() {
        return mId;
    }

    public Rectangle getBounds() {
        return mBounds;
    }

    public LookState getLookState() {
        return mLookState;
    }

    public TextureRegion getTextureRegion() {
        return mTextureRegion;
    }
}
