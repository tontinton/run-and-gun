package com.tony.runandgun.screens;

/**
 * Created by Tony Solomonik on 08/07/2016.
 */
public enum ScreenType {

    MAIN_MENU {
        public AbstractScreen getScreen(Object... params) {
            return new MainMenuScreen();
        }
    },
    GAME {
        public AbstractScreen getScreen(Object... params) {
            return new GameScreen();
        }
    };

    public abstract AbstractScreen getScreen(Object... params);
}
