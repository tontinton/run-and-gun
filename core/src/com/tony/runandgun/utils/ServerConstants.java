package com.tony.runandgun.utils;

public class ServerConstants {

    public static final String TAG = "SocketIO";

    public static final String ID = "id";
    public static final String X = "x";
    public static final String Y = "y";
    public static final String ANGLE = "angle";
    public static final String DEFAULT_ID = "0";

    /**
     * The side:
     *
     * 0 - right
     * 1- left
     */
    public static final String SIDE = "side";

    public class Functions {
        public static final String SOCKET_ID = "socketID";
        public static final String NEW_PLAYER = "newPlayer";
        public static final String PLAYER_DISCONNECTED = "playerDisconnected";
        public static final String GET_PLAYERS = "getPlayers";
        public static final String PLAYER_MOVED = "playerMoved";
        public static final String PLAYER_SHOT = "playerShot";
        public static final String PLAYER_HIT = "playerHit";
        public static final String BULLET_REMOVED = "bulletRemoved";
        public static final String PLAYER_SWITCH_SIDE = "playerSwitchSide";
    }


}
